'''
The MIT License (MIT)

Copyright (c) 2016 Ernestas Kulik

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.
'''

import dbus
import weechat

def get_playing(data):
    bus = dbus.SessionBus()

    for name in bus.list_names():
        if not name.startswith("org.mpris.MediaPlayer2."):
            continue

        player_object = bus.get_object(name, "/org/mpris/MediaPlayer2")
        player_properties = player_object.GetAll("org.mpris.MediaPlayer2.Player")

        if player_properties.get("PlaybackStatus", "Stopped") != "Playing":
            return None

        player_metadata = player_properties.get("Metadata", None)
        if not player_metadata:
            return None

        title = player_metadata.get("xesam:title", None)
        if not title:
            return None

        string = "\x02{}\x0F".format(title)

        artists = player_metadata.get("xesam:artist", None)
        if artists:
            string += " by \x02{}\x0F".format(", ".join(artists))
        album = player_metadata.get("xesam:album", None)
        if album:
            string += " on \x02{}\x0F".format(album)
        date = player_metadata.get("xesam:contentCreated", None)
        if date:
            string += " ({})".format(date.split('-', 1)[0])

    return "/me is currently playing {}".format(string)

def get_playing_cb(data, command, return_code, out, err):
    if return_code == weechat.WEECHAT_HOOK_PROCESS_ERROR:
        # Can’t get any output from get_playing(), so err will always be empty.
        weechat.prnt("", "An error occurred while running “{}”".format(command))
        return weechat.WEECHAT_RC_OK

    if out:
        weechat.command(data, out)

    return weechat.WEECHAT_RC_OK

def print_playing(data, buffer, args):
    weechat.hook_process('func:get_playing', 5000, 'get_playing_cb', buffer)

    return weechat.WEECHAT_RC_OK


weechat.register("np_mpris2",
                 "Ernestas Kulik <ernestas@baltic.engineering>",
                 "2.0",
                 "MIT",
                 "Prints currently playing media on all running players", "", "")

if __name__ == "__main__":
    weechat.hook_command("np",
            "Print currently playing media on all running players", "", "", "",
            "print_playing", "")
